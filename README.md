This topic describes how to use the AWS SAM CLI with the AWS CDK to test a Lambda function locally. For further information, see Invoking Functions Locally. To install the SAM CLI, see Installing the AWS SAM CLI.

Before starting make sure you have AWS CLI, AWS CDK & AWS SAM CLI installed and the AWS CLI has been configure using _aws configure_ command on terminal.

The first step is to create a AWS CDK application and add the Lambda package.

```
mkdir hello-cdk
cd hello-cdk
cdk init app --language typescript
npm install @aws-cdk/aws-lambda
```

Replace the contents in lib/hello-cdk-stack.ts with the following:

```
import * as cdk from "@aws-cdk/core";
import * as lambda from "@aws-cdk/aws-lambda";

export class HelloCdkStackNN extends cdk.Stack { // Replace NN with your initials to avoid naming collisions
  constructor(scope: cdk.App, id: string, props?: cdk.StackProps) {
    super(scope, id, props);
    // defines an AWS Lambda resource
    const my_lambda = new lambda.Function(this, "MyFunction", {
      runtime: lambda.Runtime.NODEJS_12_X, // execution environment
      code: lambda.Code.fromAsset("my_function"), // code loaded from "lambda" directory
      handler: "app.handler", // file is "hello", function is "handler"
    });
}
```

From the project root create the directory my_function:

```
mkdir my_function
```

Create the file app.js in my_function with the following content:

```
exports.handler = async function (event, context) {
  return {
    statusCode: 200,
    headers: { "Content-Type": "text/plain" },
    body: "This is a Lambda Function defined through CDK"
  };
};
```

Run your AWS CDK app and create a AWS CloudFormation template

```
cdk synth --no-staging > template.yaml
```

Find the logical ID for your Lambda function in template.yaml. It will look like MyFunction12345678, where 12345678 represents an 8-character unique ID that the AWS CDK generates for all resources. Run the function by executing:

```
sam local invoke MyFunction12345678 --no-event
```

Add API apigateway for CDK:

```
npm install @aws-cdk/aws-apigateway
npm update
```

Add following lines to lib/hello-cdk-stack.ts:

```
import * as cdk from "@aws-cdk/core";
import * as lambda from "@aws-cdk/aws-lambda";
import * as apigw from "@aws-cdk/aws-apigateway";

export class HelloCdkStackNN extends cdk.Stack { // Replace NN with your initials to avoid naming collisions
  constructor(scope: cdk.App, id: string, props?: cdk.StackProps) {
    super(scope, id, props);
    // defines an AWS Lambda resource
    const my_lambda = new lambda.Function(this, "MyFunction", {
      runtime: lambda.Runtime.NODEJS_12_X, // execution environment
      code: lambda.Code.fromAsset("my_function"), // code loaded from "lambda" directory
      handler: "app.handler", // file is "hello", function is "handler"
    });

    // defines an API Gateway REST API resource backed by our "hello" function.
    new apigw.LambdaRestApi(this, "Endpoint", {
      handler: my_lambda,
    });
  }
}
```

Deploy to AWS, with success you'll get and endpoint:

```
cdk synth
cdk deploy
```

Test enpoint with postman or curl (you can get the url from the cdk deploy output):

```
curl https://wo383h9re2.execute-api.eu-west-1.amazonaws.com/prod/
```
